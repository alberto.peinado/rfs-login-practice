import { UsersService } from '../../services/users/users.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  logForm: FormGroup;
  registerForm: FormGroup;
  userInvalid: boolean = false;
  passwordInvalid: boolean = false;
  display: boolean = false;

  constructor(private router: Router, private fb: FormBuilder, public userService: UsersService) {

    this.logForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.registerForm = this.fb.group({
      userName: ['', Validators.required],
      email: ['', Validators.email],
      password: ['', Validators.required],
      phone: ['', Validators.required],
      zipCode: ['', Validators.required],
      country: ['', Validators.required]
    });

  }

  ngOnInit(): void {


  }

  
  displayMessage(msg: String): void {
    
    const boxContainer = document.getElementById("message-box");
    const msgContainer = document.getElementById("box-msg");
    msgContainer.innerHTML = `${msg}`;
    
    boxContainer.style.display = "block";
    
    setTimeout(() => {
      
      boxContainer.style.opacity = "1";
      
    }, 0);
    
  }
  
  hideBox(): void {
    
    const boxContainer = document.getElementById("message-box");
    boxContainer.style.opacity = "0";
    setTimeout(() => boxContainer.style.display = "none", 1000);
    
  }
  
  onSubmit(): void {

    if (this.logForm.status === "VALID") {

      const exists = this.userService.getUserByEmail(this.logForm.value.email);

      if (exists === -1) {

        this.displayMessage("Email doesn't exist in our DB - yet.")
        return;

      }

      const checkedPass = this.userService.checkPassword(this.logForm.value.email, this.logForm.value.password);

      if (checkedPass !== -1) {

        this.router.navigate([`/welcome/${checkedPass}`]);

      }

      else {

        this.displayMessage("Password not correct.");

      }

    }

    else {

      this.displayMessage("Fill some data first. Please.");

    }

  }

  onRegister(): void {

    if(this.registerForm.status === "VALID"){

      this.display = false;
      const newUser: User = {
  
        id: -1,
        userName: this.registerForm.value.userName,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password,
        phone: this.registerForm.value.phone,
        address: {
          zipCode: this.registerForm.value.zipCode,
          country: this.registerForm.value.country
        }
  
      }

      this.userService.addUser(newUser);
      this.displayMessage("Successful registration. Log in!");
    
    } else {

      this.displayMessage("Hey, something's wrong.");

    }

  }

  showDialog(): void {

    this.display = true;

  }

}
