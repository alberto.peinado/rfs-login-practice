import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user/user';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html'
})
export class WelcomeComponent implements OnInit {

  id: number;
  user: User;

  constructor(private route: ActivatedRoute, private userService: UsersService) {

    this.user = undefined;

  }
  
  ngOnInit(): void {

    this.route.params.subscribe(p => {

      this.id = +p.id;
      console.log(`id: ${this.id}`)
      this.user = this.userService.getUserById(this.id);

    });

  }

}
