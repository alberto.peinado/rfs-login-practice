import { Address } from '../address/address';

export interface User {

    id: number,
    userName: String,
    email: String,
    password: String,
    phone: String,
    address: Address,

}
