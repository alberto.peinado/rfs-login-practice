import { Address } from '../../models/address/address';
import { User } from '../../models/user/user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: User[] = [{
      id: 0,
      userName: 'areberuto',
      email: 'areberuto@gmail.com',
      password: '1234',
      phone: '123123123',
      address: {
        zipCode: '41010',
        country: 'SPA'
      }
    }
  ];

  lastId: number = 1;

  constructor() { 

  }

  //GETTER

  get userCount(): number{

    return this.users.length;

  }

  //USER

  addUser(user: User): User[]{

    user.id = this.lastId;
    this.lastId++;
    this.users.push(user);

    return this.users;

  }

  getUserById(id: number): User {

    return this.users.find(u => u.id === id);

  }

  getUserByEmail(email: String): number {

    return this.users.findIndex(u => u.email === email);

  }

  checkPassword(email: String, password: String): number {

    const user: User = this.users[this.getUserByEmail(email)];

    return user.password === password? user.id : -1;

  }

}
